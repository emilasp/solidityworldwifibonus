var ConvertLib = artifacts.require("./ConvertLib.sol");
var WifiBonusCoin = artifacts.require("./WifiBonusCoin.sol");

module.exports = function(deployer) {
  deployer.deploy(ConvertLib);
  deployer.link(ConvertLib, WifiBonusCoin);
  deployer.deploy(WifiBonusCoin);
};
